import java.util.*;
public class Calculos{

	public static void Menu(){
		System.out.println("************** M E N U **************");
		System.out.println("   1.- Serie Fibonacci  ");
		System.out.println("   2.- Conversion Segundos    ");
		System.out.println("   3.- Salir  ");
		System.out.println("   Elige una opcion:  ");
	}
	
	public static void Serie(){
		Scanner lee=new Scanner(System.in);
		int num;
		int suma=0;
		int valor1=1;
		int valor2;
		System.out.println("¿Cuantos numeros de la serie desea ver?");
		num=lee.nextInt();
		for (int i=1; i<=num; i++) {
			valor2=suma;
			suma=valor1+suma;
			valor1=valor2;
			System.out.println(valor1);
		}
		
	}
	
	public static void Tiempo(){
		Scanner lee=new Scanner(System.in);
		int valor, horas, min, seg;
		System.out.println("Ingresa la cantidad de segundos que deseas convertir: ");
		valor=lee.nextInt();
		horas=valor/3600;
		min=(valor-(3600*horas))/60;
		seg=valor-((horas*3600)+(min*60));
		System.out.println(horas+"horas "+min+"minutos "+seg+"segundos");
	}
}